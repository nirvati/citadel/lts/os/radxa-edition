#!/bin/bash

set -eo pipefail

if [[ "$(id -u)" -ne "0" ]]; then
    echo "This script requires root."
    exit 1
fi

BOARD=$1

cat <<-EOF > /etc/radxa-device-name
$BOARD
EOF
